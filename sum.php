<?php

const INPUT_FILE = 'input.txt';
const OUTPUT_FILE = 'output.txt';

if ($argc > 1 && $argv[1] === '-h') {
    echo "Sums two numbers passed to standard input or written on first line of input.txt" . PHP_EOL .
        "Usage: php " . basename(__FILE__) . " [-]" . PHP_EOL .
        "If optional - is given, stdin is read and result is written to stdout, otherwise - " .
        "input.txt and output.txt are used" . PHP_EOL;

    exit();
}

$useStreams = $argc > 1 && $argv[1] === '-';

if ($useStreams) {
    $contents = fgets(STDIN);
} else {
    $fh = fopen(INPUT_FILE, 'r');

    if ($fh === false) {
        echo "Opening input file " . INPUT_FILE . " in read mode has failed: " . error_get_last() . PHP_EOL;
        exit(-1);
    }

    $contents = fgets($fh);
}

$args = explode(' ', rtrim($contents));

function isIntStr($val) {
    return strval((int) $val) == $val;
}

if (count($args) !== 2 || count(array_filter($args, 'isIntStr')) < 2) {
    echo "Invalid arguments. Input must be two integers";
    exit(-2);
}

$result = (int)$args[0] + (int)$args[1];

if ($useStreams) {
    echo $result . PHP_EOL;
} else {
    if (!file_put_contents(OUTPUT_FILE, $result)) {
        echo "Failed to write results to the output file " . OUTPUT_FILE . ": " . error_get_last();
    }
}