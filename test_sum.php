<?php

$tests = [
    [[0, 0], 0],
    [[-10, 10], 0],
    [[5, 2 * 10 ** 9], 5 + (2 * 10 ** 9)],
    [[-2 * 10 ** 9, 2 * 10 ** 9], -2 * 10 ** 9 + 2 * 10 ** 9],
];

foreach ($tests as $i => $test) {
    $r = [];
    exec("echo {$test[0][0]} {$test[0][1]} | php sum.php -", $r, $s);

    if ($s !== 0) {
        printf("Test {$i} failed\n");
        continue;
    }

    if ($r[0] !== (string) $test[1]) {
        printf("Failed! Expected %s, got %s\n", $test[1], $r[0]);
    } else {
        printf("%s + %s = %s\n", $test[0][0], $test[0][1], $r[0]);
    }
}

foreach ($tests as $i => $test) {
    file_put_contents("input.txt", "{$test[0][0]} {$test[0][1]}");

    exec("php sum.php", $r, $s);

    if ($s !== 0) {
        printf("Test {$i} failed\n");
        continue;
    }

    if (rtrim(file_get_contents("output.txt")) !== (string) $test[1]) {
        printf("Failed! Expected %s, got %s\n", $test[1], $r[0]);
    } else {
        printf("%s + %s = %s\n", $test[0][0], $test[0][1], $r[0]);
    }
}